---
title: Make a gpg encypted archive
---


Pack `your_dir` in a encrypted archive `your_archive.tgz.gpg` (symmetric encryption):

`tar -cz your_dir | gpg -c -o your_archive.tgz.gpg`

Unpack it:

`gpg -d your_archive.tgz.gpg | tar xz`

See the docs of GPG for how to use asymmetric instead of symmetric encryption.
