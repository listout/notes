---
title: Delete all comments on reddit
---

Delete all comments and posts

Requirements:

- Reddit Enhancement Pack

Process:

- Install the RES
- Load the comments page
- Open developer console
- Paste Code

```{.js}
var $domNodeToIterateOver = $('.del-button .option .yes'),
    currentTime = 0,
    timeInterval = 1500;
$domNodeToIterateOver.each(function() {
    var _this = $(this);
    currentTime = currentTime + timeInterval;
    setTimeout(function() {
        _this.click();
    }, currentTime);
});
```
