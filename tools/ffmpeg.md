---
title: ffmpeg nifty things
---

# Play a video

`ffplay -autoexit output.mp4`

# Play audio only

`ffplay -nodisp -autoexit output.mp4`

# Audio streaming of a youtube video

`youtube-dl https://www.youtube.com/watch?v=dQw4w9WgXcQ -f bestaudio -o - | ffplay - -nodisp -autoexit -loglevel quiet`

# Record screen and save as video

`ffmpeg -f x11grab -i :0.0 -f pulse -i 0 output.mp4`

# Record a part of the screen as gif for 5 seconds

`ffmpeg -f x11grab -framrate 10 -video_size 800x600 -i :0.0+0,30 -r 1 -t 5 output.gif`
