---
title: Firefox get sane UI
---

New firefox update has ugly UI, the following settings will make is somewhat
bare-able

In `about:config`

```
browser.proton.contextmenus.enabled = false
browser.proton.enabled = false
browser.uidensity = 1
```

From firefox 91, scroll is broken.

`mousewheel.system_scroll_override.enabled` will bring back to system default.
