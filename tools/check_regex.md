---
title: Check RegEx on Linux Terminal
---

```{.bash}
echo check_string | grep -E -x 'your regex'
```
- Replace `+` with `|`
- In case of epsilon, just leave blank.
	For example `(ε + 0 + 00)` should be `(|0|00)`
