---
title: cURL cheatsheet
---

# Options

```{.bash}
-o <file> # --output: write to file
-O # --remote-name: write output name same as the remote
-u user:pass # --user: Authentication
-v # --verbose
-vv # More verbose
-s # --silent
```

# Request

```{.bash}
-X POST # --request
-L # follow link if page redirects
```

# Data

```{.bash}
-d 'data' # --data: HTTPS post, URL encoded (eg, status="Hello")
-d @file # --data via file
-G # --get: send -d data via get
```

# SSL

```{.bash}
--cacert <file>
--capath <dir>

-E, --cert <cert> # --cert: Client cert file
	--cert-type # dar/pem/eng
-k, --insecure # for self-signed certs
```

# Headers

```{.bash}
-A <str> # --user-agent
-b name-val # --cookie
-b FILE # --header
-H "X-Foo: y" # --header
--compressed # use deflate/gzip
```

# Examples

```{.bash}
# download a file
curl -fLO URL

# post data
curl -d password=x http://x.com/y

# Auth/data
curl -u user:pass -d status="hello" http://twitter.com/statutes/update.xml

# multipart file upload
curl -v -include -from key1=value1 --from upload=@localfilename URL

# User cURL to check if remote resource is available
# details: https://matthewsetter.com/check-if-file-is-available-with-curl/
curl -o /dev/null --silent -Iw "%{http_code}" https://example.com/my.remote.tarball.gz
```
