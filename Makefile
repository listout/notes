src :=

PANDOC_OPTS += --highlight-style tango
PANDOC_OPTS += --resource-path=resources

PDF_OPTS += --pdf-engine=pdflatex
PDF_OPTS += -V geometry:margin=2cm
PDF_OPTS += -V papersize:a4
PDF_OPTS += -H header.tex

FONT_OPTS += -V fontsize=10pt

%.pdf: %.md
	pandoc $(PANDOC_OPTS) $(PDF_OPTS) $(FONT_OPTS) $< -o $@

$(src): $(wildcard $(dir $(src))*.md)
	pandoc $(PANDOC_OPTS) $(PDF_OPTS) $(FONT_OPTS) $^ -o $(src)

clean:
	find . -type f -name '*.pdf' -delete
