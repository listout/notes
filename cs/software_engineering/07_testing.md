# Testing

Coding is undertaken once the design phase is complete and the design documents
have been successfully reviewed. In the coding phase, every module specified in
the design document is coded and **unit tested** (each module is tested in
isolation from other modules). After all the module is tested independently, the
integration and system testing phase are undertaken.

During each **integration step**, a number of modules are added to the partially
integrated system and the resultant system is tested. The full product takes
shape only after all the modules have been integrated together. **System
testing** is conducted on the full product. During system testing, the product
is tested against its requirements as recorded in the SRS document.

Here we will discuss about all types of testing techniques.

- What is testing?

Testing is an important development phase and typically requires the maximum
effort among all the development phases.

Once source code has been generated, software must be tested to uncover(and
correct) as many errors as possible before delivery to the customer. So, the
goal of this phase is to design a series of test cases that have a high
likelihood finding errors.

- Why design test cases?

Usually the input data domain of most programs is very large and it is not
practical to test the program exhaustively with respect to each value. But it
not be sufficient to test a software using a large number of random input
values.

When test cases are designed based on random input data, it does not guarantee
that all (or even most) of the errors in the system will be uncovered.

Example: Consider a code segment which determines the greater of two integer
values X and Y.

```
if (X > Y)
	Max = X;
else
	Max = Y;
```

For the given test segment let one of the suit { (X=3, Y=2); (X=2,Y=3)} can
detect the error.

Where as larger test suite {(x=3,y=2), (x=4,y=3), (x=5,y=1)} does not defect the
error.

So it would be incorrect to say that a larger test suit would always detect more
errors than a smaller one, unless of course the larger test suite has also been
carefully designed. This implies that for effective testing the test suite
should be carefully designed rather than picked randomly.

Note: To satisfactorily test a software with minimum cost, we must design a
minimal test suite that is of reasonable size and can uncover as many existing
errors in the system as possible.

- Who does the testing operation?

In a typical development organization, at any time, the maximum number of
software engineers can be found to be engaged in testing activities.

**Basic Concepts and Terminologies**:

- An error/fault/bug is a mistake committed by the development team
during any of the development phases.

- A failure is the symptom of an error.  A test case is the triplet(I,S,O)

	- I -&gt; data input to the system.
	- S -&gt; State of the system at which the data is input.
	- O -&gt; Expected output of the system.

- A test suit is the set of all test cases with which a given software product
is tested.

**Testing Objectives**:

1. Testing is a process of executing a program with the intent of finding an
   error.
2. A good test case is one that has a high probability of finding an as-yet-
   undiscovered error.
3. A successful test is one that uncovers an as-yet-undiscovered error,

**Test Activities**:

Testing involves performing the following main activities:

1. Test Suite Design: The set of test cases using which a program is to be
   tested is designed using different test case design technique.
2. Running test cases and checking the results to detect failure. The mismatch
   between expected result and the actual result indicates failure.
3. Debugging: Identify the statements that are in error.
4. Error Correction.

**Testing Principles**:

Before applying methods to design effective test cases, a software engineers
must understand the basic principles that guide software testing.

- All tests should be traceable to customer requirements.
- Tests should be planned lone before testing begins.
- The pareto principle applies to software design. Stated simply the pareto
principle implies that 80% of all errors uncovered during testing will likely be
traceable to 20 percent of all program components.
- Testing should begin “in the small” and progress toward testing "in the
large".
- Exhaustive testing is not possible.
- To be most effective, testing should be conducted by an independent third
party.

**Testability**:

Software testability is simply how easily a program can be tested.

Properties of good testability:

- *Operability*: "The better it works, the more efficiently it can be tested".
	- The system has few bags.
	- no bugs block the execution of tests.

- *Observability*: “What you see is what you test.”
	- Distinct output is generated for each input.
	- Incorrect output is easily identified.
	- Internal errors are automatically detected through self testing
	mechanisms.
	- Internal errors are automatically reported.

- *Controllability*: "The better we can control the software, the more tests
can be automated and optimized".
	- All possible outputs can be generated through some combinations of
	input.
	- All code is executable through some combination of input.
	- Input and Output formats are consistent and structured.
	- Decomposability: - "By controlling the scope of testing we can more
	quickly isolate problems and perform smaller testing." The software system
	is built from independent modules. Software modules can be tested
	independently.

- *Simplicity*:- Functional Simplicity, Structural simplicity, Code simplicity.

- *Stability*: "The fewer the changes, the fewer the disruptions to testing".
	a. Changes to the software are infrequent.
	b. Changes to the software are controlled.
	c. The software recovers well from failure.

- *Understandability*: "The more information we have, the smaller we will
test". The design is well understood. Dependencies between internal and external
and shared components are well understood. Testing documentation is specific,
detailed and accurate.

**Test Case Design**:

There are essentially two main approaches to systematically design test cases:
**Black-Box approach**, **White-box**(or glass box) approach.

In this black box approach test cases are designed using only the functional
specification of the software. That is, test cases are designed based on an
analysis of the input/output behavior (**functional behavior**) and does not
require any knowledge of the internal structure of a program. For this reason,
black box testing is also known as functional testing.

Designing white-box test cases requires a **thorough knowledge of the internal
structure of a program**, and therefore white box testing is also called
structural testing. White box test cases are based on an analysis of the code.

- A software product is normally tested in three levels or stages:

	- Unit Testing
	- Integration Testing
	- System Testing

During Unit testing, the individual components (or units) of a program are
tested. Unit testing is referred to as testing in the small, whereas integration
and system testing are referred to as testing in the large.

After testing all the units individually, the units are slowly integrated and
tested after each step of integration (integration testing). Finally the fully
integrated system is tested (system testing). Integration and system testing are
known as testing in the large.

**Unit Testing**:

Unit testing is undertaken after a module has been coded and reviewed. Before
carrying out unit testing, the unit test cases have to be designed and the test
environment for the unit under test has to be developed.

**Driver and Stub Modules**:

In order to test a single module, we need a complete environment to provide all
relevant code that is necessary for execution of the module. That is, besides
the module under test, the following are needed to test the module.

- The procedure belonging to other modules that the module under test calls.
- Nonlocal data structures that the module access.
- A procedure to call the functions of the module under test with appropriate
parameters.

Modules required to provide the necessary environment (which either call or
called by the module under test) are usually not available until they too have
been unit tested. In this context stubs and drives are designed to provide the
complete environment for a module so that testing can be carried out.

*Stub*:

The role of stub and driver modules is pictorially shown in below figure. A stub
procedure is a dummy procedure that has the same I/O parameters as the given
procedure, but has a highly simplified. For example, a stub procedure may
produce the expected behavior using a simple table look up mechanism.

*Driver*:

A driver module should contain the non-local data structures accessed by the
module under test. Additionally, it should also have the code to call the
different functions of the module under test with appropriate parameters values
for testing.

![Stub and Driver module example](gv/seng4.png){height=20%}

**Black-Box Testing**:

In black box testing, test cases are designed from an examination of the
input/output values only and no knowledge of design or code is required. The
following are two approaches available to design black-box test cases:

- Equivalence Class partitioning.
- Boundary value partitioning

**Equivalence Class Partitioning**: In the equivalence partitioning approach,
the domain of input values to the program under test is partitioned in to set of
equivalence classes. The partitioning is done such that for every input data
belonging to the same equivalence class, the program behaves similarly.

Example: For a software that computes the square root of an input integer that
can assume value in the range 0 and 500. Determining the equivalence class test
suite.

There are 3 equivalence class

1. The set of negative integers.
2. The set of integers in the range 0 to 500.
3. The set of integers larger than 500. A possible test suite can be {-5, 500,
   6000}

Example: Design equivalence class partitioning test suite for a function that
reads a character string of size less than 5 characters and displays whether it
is a palindrome.

The equivalence classes are palindromes, non-palindromes and invalid inputs. We
have the required test suite {abc, aba, abcdef}.

**Boundary value analysis**:

Boundary value analysis is a test case design technique where rather than
selecting any element of an equivalence class, Boundary Value Analysis leads to
selection of test cases at the edge of the class.

Example: For a function that computes the square root of the integer values in
the range 0 to 500, determine the boundary value test suite.

The test suite is {0, -1,500,501}

**White Box Testing**:

White box testing is an important type of unit testing. A large number of white-
box testing strategies exist. Each testing strategy essentially designs test
cases based on analysis of some aspect of source code and is based on some
heuristic.

A white box testing strategy can either be **coverage-based** or **fault
based**. A fault-based testing strategy targets to detect certain types of
faults. Example Mutation testing. A coverage-based testing strategy attempts to
execute (or cover) certain elements of program. Example: statement coverage,
branch coverage and path coverage-based testing.

The set of specific program elements that a testing strategy requires to be
executed is called the testing criterion of the strategy.

- **Stronger Vs Weaker testing**: A white box testing strategy is said to be
stronger than another strategy if all types of program elements covered by the
second testing strategy are also covered by the first testing strategy and the
first testing strategy additionally covers some more types of elements not
covered by the second strategy.

When none of two testing strategies full covers the program elements exercised
by the other, then the two are called complementary testing strategies.

If a stronger testing has been performed, then a weaker testing need not be
carried out.

A test suit should, however be enriched by using various complementary
testing strategies.

- **Coverage based testing strategies**:

1. Statement Coverage.
2. Branch Coverage.
3. Path Coverage.

**Statement Coverage**: The statement coverage-based strategy aims to design
test cases so as to execute every statement in a program at least once.

Weakness of the statement coverage strategy is that executing a statement once
and observing that it behaves properly for one input values is no guarantee that
it will behave correctly for all input values.

Example:

```{.c}
int computeGCD(x, y) {
	while(x != y) {
		if (x > y) x = x + y
		else y = y - x
	}
return x
}
```
By choosing the test set {(x=3, y=3), (x=4, y=3), (x=3, y=4)} all statements of
the program would be executed atleast once.

**Branch Coverage**: Branch Coverage based testing requires test cases to be
designed so as to make each branch condition in the program to assume true and
false values in turn. Branch testing is also known as edge testing, since in
this testing schema each edge of a program’s control flow graph is traversed
atleast once.

For GCD program test cases for branch coverage can be
{(x=3,y=3),(x=3,y=2),(x=4,y=3),(x=3,y=4)}.

Branch coverage based testing is stronger than statement coverage based testing.

**Condition Coverage**: In the condition coverage based testing case, test cases
are designed to make each component of a conditional expression to assure both
true and false. Example conditional expression ((c1 and c2)or c3) the components
c1, c2 and c3 are each made to assume both True and False always.

For a composite conditional expression of n components 2 n test cases are
required for condition coverage. Thus for condition coverage, the number of test
cases increase exponentially with the number of component conditions.  Therefore
a condition coverage-based testing technique is practical only if n (number of
condition) is small.

It is easy to prove that condition coverage based testing is a stronger testing
strategy than branch testing.

**Path Coverage**: Path coverage based testing strategy requires designing test
cases such that all linearly independent paths(or basic paths) in the program
are executed atleast one. A linearly independent path can be defined in terms of
the *Control Flow Graph*(CFG) of a program.

**Control Flow Graph**: A control flow graph describes the sequence in which the
different instructions of a program get executed. *To draw a control flow graph
first number all the statements of a program. The different numbered statements
serve as nodes of the CFG. There exist an edge from one node to another, if the
execution of the statement representing the first node can result in the
transfer of control to the other node.*

If we know how to represent the sequence, selection and iteration types of
statements in the CFG, then we can draw CFG for all types of programs because
program is constructed from these 3 types of constructs only.

Sequence:

1. a = 5;
2. b = a*2-1;

![Sequence Example](gv/seng5.png){height=20%}

Selection:

1. if (a > b)
2. c = 3
3. else c = 5
4. c = c* c

![Selection Example](gv/seng6.png){height=20%}

Iteration:

1. while(a > b) {
2. b = b - 1
3. b = b * a }
4. c = a + b

![Iteration Example](gv/seng7.png){height=20%}

Problem : Draw a CFG graph for the GCD program.

```
int computeGCD(int x, int y) {
	1. while(x != y) {
		2. if ( x > y ) then
		3. x = x - y
		4. else y = y - x
		5.  }
		6. return x
}
```

![GCD control flow graph](gv/seng8.png){height=30%}

Path: A path through a program is any node and edge sequence from the start node
to a terminal node of the CFG of a program in the presence of return or exit
types of statements.

Writing test cases to cover all paths of typical program is impractical since
there can be an infinite number of paths through a program in presence of loops.
For example in iteration based CFG graph has infinite number of paths like
1-2-3-4, 1-2-3-1-2-3-4, 1-2-3-1-2-3-1-2-3-4 etc.

For this reason, path coverage based testing has been designed, to not to cover
all paths, but only a subset of paths called linearly independent path (or basis
paths)

**How to identify linearly independent paths (or basis path set)**: A set of
paths for a given program is called linearly independent set of paths, if each
path in the set introduces atleast one new edge that is not included in any
other path in the set.
