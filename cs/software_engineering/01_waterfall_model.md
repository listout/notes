---
title: \huge Software Engineering
documentclass: report
---

# Waterfall Model

## Phases

- Feasibility Study
- Requirement Analysis and Specification
- Design
- Coding and Unit Testing
- Integration and System Testing
- Maintenance

![Waterfall Model](gv/seng1.png){width=50%}

The phases starting from feasibility study to system and integration testing is known as the **Development Phase**. The software is developed during the development phase, at the end of the development phase of the life cycle the product becomes ready to be delivered to the customers.

The maintenance phase starts after completion of the development phase.

## Feasibility Study

The main aim of the **Feasibility Study** is to determine whether it would be functionally and technically possible to develop the project.

For these we gather all important requirement and details of requirement of ignored.

Analyze the possible ways of solution so that the cost and time is minimal. Also analyze how many number of resources are required. If there is resources constraint or cost is too high then it may determined that none of the solution is feasible due to high cost or resources constraint.

## Requirement Analysis

The aim of requirement analysis and specification phase is to understand the exact requirements of the customer and to document them properly.

This phase consists of two distinct activities:

- Requirement gathering and analysis
- Requirement specification (documentation $\to$ SRS documentation)

## Design Phase

The goal of the design phase is to transform the requirement specified in the SRS documentation into a structure that is suitable for
implementation in some programming language.

Structured Design consists of two main activities.

- Architectural design (high level design)
- Detailed design (low level design)

**High Level**: design involves decomposition of the symbol into modules, representation of the interfaces, and invocation relationship
among modules.
It is also known as software network.

During **detailed design** internals of the individual modules are designed in greater details. For example the data structure and
algorithms are designed and documented.

## Coding

The purpose of the coding phase is to translate the software design into source code. The coding phase is also known as implementation
phase, since the design is implemented into a workable in this phase.

Each component of the design is implemented as the program module.

The end product of this phase is a set of program modules that have been individually tested.

## Testing

Once the programming is completed the code is integrated and testing is done. Upon successful completion of the testing the
system is installed.

There are different types of testing:

- Unit testing
- System testing (alpha and beta)
- Integration testing
- Acceptance testing

## Maintenance

Maintenance involves performing any one or more of the following three kind of activities:

- **Corrective Maintenance**: These type of maintenance involves correction errors that were not discovered
during the product development phase
- **Perfective Maintenance**: These type of maintenance involves improving the imperfections of the system
and enhancing the functionalities of the system according to customers requirement.
- **Adaptive Maintenance**: Usually required for porting the software to work in new environment.

## Advantages of Waterfall model

- Easy to understand.
- Well suited for simple purpose.
- Suitable for development project.
