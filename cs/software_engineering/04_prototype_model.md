# Prototype Model

A prototype usually turns out to be a very crude version of the actual
system, possibly exhibiting limited functional capabilities, low
reliability and inefficient performance as compared to the actual software.

For many types of projects, there are several advantages of building a
prototype before the actual product is developed. An important use of a
prototype can be to illustrate the input data formats, messages, reports
and the interactive dialogues to the customer. This can be a valuable
mechanism for gaining better understanding of the customer's needs.
In this regard, the prototype model turns out to be especially
useful in developing the graphical user interface(GUI) part of a system.
For the user, it becomes much easier to form his opinion by experimenting
with a working model, rather than trying to imagine the working of
a hypothetical system.

![Prototype Model](cs/prototype_model.png){width=50%}

The first phase is prototyping development to control various risks.
This is followed by an iterative development cycle. In this model,
prototyping starts with initial requirements gathering phase. A quick
design is carried out and a prototype is built. The developed prototype
is submitted to the customer for his evaluation. Based on the customer
feedback, the requirements are refined and the prototype is suitably
modified. This cycle of obtaining customers feedback and modifying
the prototype continues till the customer approves the prototype.

Once the customer approves the prototype the actual system is
developed using the iterative waterfall approach. In spite of
the availability of a working prototype, the SRS document is
required to be developed for carrying out traceability analysis,
verification and test case design during later phases.

However especially for developing GUI part using the
prototyping model, the requirements analysis and specification
phase would become redundant since the working prototype that
has been approved by the customer can serve as an animated
requirements specification.

## Advantages:

- User can interact easily and hence requirement gathering is easy.
- Development team also understand better through the prototype.

## Disadvantages:

- Non sequential leads version management problem.
- The overall design is not the best and optimized.
