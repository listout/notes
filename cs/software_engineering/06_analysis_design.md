# Analysis and Design

During the software design phase, the design document is produced, based on the
customer requirements as documented in the SRS document.

## Main Objective of the design phase:

The activities carried out during the design phase (called the design process)
transform the SRS document in to the design document. The design document
produced at the end of the design phase should be implementable using a
programming language in the subsequent (coding) phase.

- During design we determine what modules should the system have and which have
to be developed. That is, the design exercise determines the module structure
of the components.
- The design of a system is essentially a blue print or a plan for a solution for
the system. Here we consider a system to be a set of modules with clearly
defined behavior which interact with each other in a defined manner to produce
some behavior or services for its environment.

## Outcome of Design Process:

- Different Modules required: The different modules in the solution be clearly
identified. Each module is a collection of functions and data shared by the
functions of the module. Each module should accomplish some well defined task
out of the overall responsibility of the software.
- Control relationship among modules: A control relationship between two modules
essentially arises due to function calls between the two modules. The control
relationships existing among various modules should be identified in the design
document. The relationship must be simple and a module consists all the
functions which are related to each other.
- Interfaces among different modules: the interfaces between two modules
identifies the exact data items exchanged between the two modules when a one
module invokes a function of the other module.
- Data Structure of the individual modules.
- Algorithm required to implement the individual modules.

The design documents are reviewed by the members of the development team to
ensure that the design solution conforms to the requirements specification.

## Levels of Design Activities:

**First Level**: Preliminary design/ High level design/ Top-level design/ System
design

**Second Level**: Detailed Design/ Logic Design

1. High Level Design:

Through high level design, a problem is decomposed in to a set of modules, the
control relationships among various modules identified and also the interfaces
among various modules are identified. At the first level the focus is on
deciding which modules are needed for the system and how the modules should be
interconnected.

2. Detailed Design:

During detailed design, each module is examined carefully to design its data
structure and the algorithms. Outcome is module specification.

Note: High level design is crucial step in the overall design of a software.
After the high level design is complete, we have managed to decompose the
problem in to small functionally independent modules that are cohesive and have
low coupling among themselves. The notation which is widely used to represent
high level design is a tree like diagram called the structure chart.

## Analysis Vs Design

- The goal of any analysis technique is to elaborate the customer requirements
through careful thinking and at the same time consciously avoiding making any
decisions regarding the exact way the system is to be implemented.

1. First, in the problem analysis, we are constructing a model of the problem
   domain, while in design we are constructing a model for the solution domain.
2. The analysis model usually documented using some graphical formalism. In
   case of the function-oriented approach, the analysis model would be
   documented using Data Flow Diagram(DFD) where as the design would be
   documented using Structure Chart.
3. The basic aim of modeling in problem analysis is to understand, while the
   basic aim of modeling in design is to optimize.
4. In design, the system depends on the model, while in problem analysis the
   model depends on the system.

- The design model is obtained from the analysis model through transformations
over a series of steps. In contrast to the analysis model, the design model
reflects several decisions takes regarding the exact way system is to be
implemented. The design model should be detailed enough to be easily
implemented using a programming language.

## How can we characterize a good software design?

1. Correctness: A good design should first of all be correct. That is, it
   should be correctly implemented all the functionalities of the system.
2. Understandability: A good design should be easily understandable. Otherwise
   it would be difficult to implement and maintain it.
3. Efficiency: A good design solution should adequately address resource, cost
   and time optimization issues.
4. Maintainability: A good design should be easy to change. This is an
   important requirement, since change requests usually keep coming from the
   customer even after product release.

## A design solution is understandable, if it is modular and the modules are arranged in layers.

1. Modularity:  A modular design achieves effective decomposition of a problem.
   A modular design, in simple words implies that the problem has been
   decomposed in to a set of modules. If different modules have either no
   interactions or little interactions with each other, then each module can be
   understood separately. This reduces the perceived complexity of the design
   solution greatly.  For example, consider two  alternate design solutions to
   a problem that are represented in fig, in which the modules M1 , M2 etc has
   been drawn as rectangles. The invocation of a module by another module has
   been shown as an arrows.

![Example](gv/seng3.png){width=50%}


It can easily be seen that the design solution of figure one would be easier to
understand since the interactions among the different modules is low.

But how we quantitatively measure the modularity of a design solution, it will
be hard to say which design solution is more modular than other. Unfortunately,
there are no quantitative metrics available yet to directly measure the
modularity of a design. However, we can quantitatively characterize the
modularity of a design solution based on the cohesion and coupling existing in
the design.

**A design solution is considered to be highly modular if the different modules
in the solution have high cohesion and their inter-module couplings are low.**

2. Layered design: A layered design is one in which when the call relations
   among different modules are represented graphically, it would result in a
   tree like diagram with clear layering. In a layered design solution, the
   modules are arranged in hierarchy of layers. A module can only invoke
   functions of the modules in the layer immediately below it. The higher layer
   modules can be considered to be similar to manager that invoke(order) the
   lower layer modules to get certain tasks done.

## Cohesion and Coupling:

Cohesion is a measure of the functional strength of a module where as the
coupling between two modules is a measure of the degree of interaction (or
interdependence) between two modules.

**Coupling**: Two modules are said to be highly coupled, in either of the following
two situations arises.

- If the function calls between two modules involve passing large chunks of
shared data, the modules are tightly coupled.
- If the interactions occur through some shared data, then we say that they are
highly coupled.

If two modules either do not interact with each other at all or at best
interact by passing no data or only a few primitive data items they are said to
have low coupling.

## Classification of Coupling

The degree of coupling between two modules depends on their interface
complexity. Interface complexity based on the number of parameters and the
complexity of the parameters that are interchanged while one module invokes the
functions of the other module.

1. Data coupling: Two modules are data coupled if they communicate using an
   elementary data item that is passes as a parameter between the two, example
   an integer, a float, a character etc.
2. Stamp coupling: Two modules are stamp coupled if they are communicate using
   a composite data item such as structure in C.
3. Control Coupling: Control coupling exists between two modules, if data from
   one module is used to direct the order of instruction execution in another.
   An example, of control coupling is a flag set in one module and tested in
   another module.
4. Common Coupling: Two modules are common coupled, if they share some global
   data items.
5. Content Coupling: Content coupling exists between two modules, if they share
   code. That is, a jump from one module in to the code of another module can
   occur.

![Coupling Classification](cs/coupling_classification.png){width=50%}

High coupling among modules not only makes a design solution difficult to
understand and maintain, but it also increases development effort and also
makes it very difficult to get these modules developed independently by
different team members.

**Cohesion**: Cohesion is a measure of the functional strength of the module.
Cohesiveness of a module is the degree to which the different function of the
module cooperate to work towards a single objective.

## Classification of Cohesion

1. Coincidental Cohesion: A module is said to have coincidental cohesion, if it
   performs a set of tasks that relate to each other very loosely, it at all.
   In this case, we can say that the module contains a random collection of
   functions.

   Example: Module name: Random Operations

   Function:

	```
	Issue: Book
	Create_Member
	```

2. Temporal Cohesion: When a module contains functions that are related by the
   fact that these functions are executed in the same time span, then this
   module is said to posses temporal cohesion.

   Example, When a computer is booted, then memory and devices are initialized,
   loading the operating system etc. When a single module performs all these
   tasks, then the module can be said to exhibit temporal cohesion.

3. Logical Cohesion: A module is said to be logically cohesive, if all elements
   of the module perform similar operations such as error handling, data input,
   data output etc.

   Example, A module that contains a set of print functions to generate various
   type of output reports such as grade sheets, salary slip, annual reports
   etc.

4. Procedural Cohesion: A model is said to possess procedural cohesion if the
   set of functions of the modules are executed one after the other, through
   these functions, may work entirely different purpose and on different data
   items.

   Example:

	```
	login()
	Place_Order()
	Print_Bill()
	Place_order_on_vendor()
	Update()
	logout()
	```

5. Communicational Cohesion: A model is said to have communicational cohesion,
   if all functions of the module refer or update the same data structure.

   Example: Module Name: Student

   Functions:

	```
	Admit_Students()
	Enter_Marks()
	PrintGradeSheet()
	```

   All these functions access and manipulate data stored in an array named
   STUDENT_RECORDS define within the module.

6. Sequential Cohesion: A module is said to possess sequential cohesion, if the
   different function of the module executes in a sequence, and the output from
   one function is input to the next in the sequence.

   Example: In an online store, consider that after a customer requests for
   some item, it is first determined if the item is in stock. In this case, if
   the function create_order(), check_item_avaliability(), place_order() are
   placed in a single module, then the model would exhibit sequential
   cohesion.

7. Functional Cohesion: A module is said to possess functional cohesion, if
   different functions of the module cooperate to complete a single task.

   Example: Managing_Book_Lending

   Function:

	```
	Issue_Book()
	Return_Book()
	Query_Book()
	Find_Borrower()
	```

   In this example, the functions together manage all activities concerned with book lending.
