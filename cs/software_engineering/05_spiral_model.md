# Spiral Model

The diagrammatic representation of this model appears like a spiral with many
loops. The exact number of loops of the spiral is not fixed and can vary from
project to project. Each loop of the spiral is called a phase of the software
process. It can be seen that this model is much more flexible compared to the
other models, since the exact number of phases through which the product is
developed is not fixed.

Over each loop, one or more features of the product are
elaborated and analyzed and the risks at that point of time are identified and
are resolved through prototyping. Based on this, the identified features are
implemented.

![Spiral Model](cs/spiral_model.png){width=50%}

[A risk is essentially any adverse circumstance that might hamper the successful
completion of a software project.]

## Phases of Spiral Model:

- Each phase in this model is split in to four sectors(or quadrants).

- In the first quadrant, some features of the product are identified based on
the severity of the risk and how crucial it is to the overall product
development. Implementation of the identified features forms the objective of
the phase. The objectives are investigated, elaborated and analyzed.

- During the second quadrant, the alternative solutions are evaluated to select
the best possible solution. To be able to do this, the solutions are evaluated
by developing an appropriate prototype.

- Activities during the quadrant consist of developing and verifying the next
level of the product. At the end of the third iteration, the identified
features have been implemented and the next version of the product is
available.

- Activities during the fourth quadrant concern reviewing the results of the
stages traversed so far(i.e. the next version of the product) with the customer
and planning the next iteration around the spiral.

## Advantages:

- Incorporates the best of waterfall and prototype.
- Handle risk.
- Analysis alternatives.

## Disadvantages:

- Needs experienced people to do proper risk analysis.
- Lot of efforts needed for analysis alternatives.
