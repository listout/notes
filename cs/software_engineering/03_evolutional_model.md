# Evolutionary Model

This lifecycle model is also referred to as the **successive versions model** and sometimes as
the **incremental model**. In this life cycle model, first a simple working system is built,
which subsequently undergoes many functionality improvements and additions until the
desired system is realized. The evolutionary software development process is therefore
sometimes referred to as **design a little, build a little, test a little, deploy a
little model**. That is, once the requirements have been specified, the design, build,
test and deployment activities are interleaved.

## Lifecycle Activities

- The software requirement is first broken down into several modules(or functional units)
that can be incrementally constructed and delivered.
- The development team first develops the core modules of the system(The core modules
are those that do not need services from the other modules.On the other hand non-core
modules need services from the core modules.).
- The initial product skeleton is refined into increasing levels of capability by
adding new functionalities in successive versions.
- Each evolutionary version may be developed using an iterative waterfall model of development.
- Each successive version of the product is a fully functioning software capable of
performing more work that the previous versions.

![Evolutionary Model of Software Development](gv/seng2.png){width=50%}
