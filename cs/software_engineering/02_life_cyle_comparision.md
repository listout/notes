# Comparison of Different Life Cycle Models

- The classical waterfall model can be considered as the basic model but cannot be used in practical
development projects, since this model supports no mechanism to correct the errors that are
committed during any of the phase but detected at a later phase.

- The problem of classical waterfall model is overcome by the Iterative Waterfall Model through
the provision of feedback paths. This model is the most widely used software development model
so far, it is easy to understand but this model is suitable only for well-understood problem and not
suitable for very large projects and for projects that suffer from many types of risks. If the
software is a simple data processing application, then the Iterative Waterfall model should be
sufficient. If development team is experienced in developing similar products, then even an
embedded system can be developed using an iterative waterfall model.

- The Prototyping model is suitable for the projects for which either the user requirements or the
underlying technical aspects are not well understood, however all the risks can be identified
before the project starts. This model is especially popular for development of the user interface
parts of projects. So this model can be used where risks are few and can be determined at the start
of the project. If the development team is entirely novice, then even a simple data processing
application may require a prototyping model to be adopted. If the customer is not quite familiar
with computers, then the requirements are likely change frequently as it would be difficult to
form complete, consistent and unambiguous requirements. Thus a prototyping model may be
necessary to reduce the later change requests from the customers.

- The evolutionary approach is suitable for large problems which can be decomposed in to a set of
modules for incremental development and delivery. This model is also used widely for object
oriented development projects. Of course, this model can only be used if incremental delivery of
the system is acceptable to the customer.

- The spiral model is considered a meta model and encompasses all other life cycle models.
Flexibility and risk handling are inherently built into this model. The spiral model is suitable for
development of technically challenging and large software products that are prone to several
kinds of risks that are difficult to anticipate at the start of the project. However this model is much
more complex than the other model. So this model is useful when the risks are difficult to
anticipate at the beginning of the project, but are likely to crop up as the development proceeds.
