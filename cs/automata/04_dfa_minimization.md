# Minimization of Finite Automata

- Two states $q_1$ and $q_2$ are equivalent if both $\delta(q_1, x)$ and $\delta(q_2, x)$ are final states or both of them are nonfinal states for all x $\in \sum^*$
- Two states $q_1$ and $q_2$ are equivalent $(k \geq 0)$ if both $\delta(q_1, x)$ and $\delta(q_2, x)$ are final states or both nonfinal $\forall$ x of length k or less.

## Algorithm

- Determine final states
- Remove all states which are not reachable from initial state
- Draw state transition table
- Find out 0 equivalence state ( separate steps for final and nonfinal states )
- Find out 1 equivalence state
- Find out 2 equivalence state
- Repeat until partition not possible.
