# Nondeterministic Finite Automata

## Definition

A nondeterministic finite automaton (NDFA) is a 5-tuple $(Q, \sum, \delta, q_0, F)$, where

- Q is a finite nonempty set of states
- $\sum$ is a finite nonempty set of inputs
- $\delta$ is transition function mapping from $Q \times \sum$ into $2^Q$ which is the power of Q, the set of all subsets of Q
- $q_0 \in Q$ is the initial state
- $F \subseteq Q$ is the set of final states.

## Different between deterministic and nondeterministic automata

The difference between the deterministic and nondeterministic automata is only in $\delta$. For deterministic automata the outcome is a state i.e. an element of $Q$; for nondeterministic automaton the outcome is a subset of $Q$

## NDFA Examples

- NDFA that accepts all stings starting with `a`

![](gv/nfa1.png){width=50%}

## NDFA to DFA Conversion

- Construct a DFA, $M = ( \{ q_0, q_1 \}, \{a_1\}, \delta, q_0, \{ q_0 \} )$

| state       | 0       | 1          |
| ----------- | ------- | ---------- |
| $\to q_0$   | $q_0$   | $q_1$      |
| $q_1$       | $q_1$   | $q_0, q_1$ |

Table: NFA Table

| state       | 0           | 1           |
|-------------|-------------|-------------|
| $\phi$      | $\phi$      | $\phi$      |
| $[q_0]$     | $[q_0]$     | $[q_1]$     |
| $[ q_1 ]$   | $[q_1]$     | $[q_0 q_1]$ |
| $[q_0 q_1]$ | $[q_0 q_1]$ | $[q_0 q_1]$ |

![](gv/dfa17.png){width=50%}

- NDFA for all strings in which $2^{nd}$ symbol from right is `a`

![](gv/dfa18.png){width=50%}

| state | a         | b      |
|-------|-----------|--------|
| $q_0$ | $q_0 q_1$ | $q_0$  |
| $q_1$ | $q_2$     | $q_2$  |
| $q_2$ | $\phi$    | $\phi$ |

| state     | a             | b         |
|-----------|---------------|-----------|
| $q_0$     | $q_0 q_1$     | $q_0$     |
| $q_1$     | $q_2$         | $q_2$     |
| $q_1 q_2$ | $q_0 q_1 q_2$ | $q_0 q_2$ |

![](gv/dfa19.png){width=50%}

- NDFA for all strings which has $3^{rd}$ symbol from right as `a`

![](gv/dfa20.png){width=50%}

| state  | a       | b       |
|--------|---------|---------|
| $\phi$ | $\phi$  | $\phi$  |
| $q_0$  | $[q_0]$ | $[q_0]$ |
| $q_1$  | $[q_2]$ | $[q_2]$ |
| $q_2$  | $[q_3]$ | $[q_3]$ |

| state             | a                 | b             |
|-------------------|-------------------|---------------|
| $q_0$             | $q_0 q_1$         | $q_0$         |
| $q_0 q_1$         | $q_0 q_1 q_2$     | $q_0 q_2$     |
| $q_0 q_2$         | $q_0 q_1 q_3$     | $q_0 q_3$     |
| $q_0 q_3$         | $q_0 q_1$         | $q_0$         |
| $q_0 q_1 q_2$     | $q_0 q_1 q_2 q_3$ | $q_0 q_2 q_3$ |
| $q_0 q_1 q_3$     | $q_0 q_1 q_2$     | $q_0 q_2$     |
| $q_0 q_2 q_3$     | $q_0 q_1 q_3$     | $q_0 q_3$     |
| $q_0 q_1 q_2 q_3$ | $q_0 q_1 q_2 q_3$ | $q_0 q_2 q_3$ |

![](gv/dfa21.png){width=50%}
