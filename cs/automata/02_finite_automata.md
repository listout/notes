# Finite Autmaton

A finite automaton is represented by five tuples $(Q, \sum, \delta, q_0, F)$ where

- Q is a finite nonempty set of states
- $\sum$ is a finite nonempty set of inputs called the *input alphabet*
- $\delta$ is a function which maps $Q \times \sum$ into Q and is usually called the *direct transmission function*. This map is usually represented by a transition table or a transition diagram.
- $q_0 \in Q$ in the initial state
- $F \subseteq Q$ is the set of final states. It is assumed here that there may be more than one final state.

## Property of Transition Function

- Property 1: $\delta(q, A) = q$ is a finite automaton. This means that the state of the system can be changed only by an input symbol.
- Property 2: For all strings $w$ and input symbols $a$
	- $\delta(q, aw) = \delta(\delta(q, a), w)$
	- $\delta(q, wa) = \delta(\delta(q, w), a)$

	This property gives the state after the automaton consumes or reads the first symbol of a string $aw$ and the state after the automaton consumes a prefix of the string $wa$.

## DFA Construction

- Construct a DFA that accepts set of all strings over $\sum = {a,b} \ \mid \ w \ = 2$

![](gv/dfa1.png){width=50%}

| Present State   | Next state for Input a   | Next State for Input b   |
| :-------------: | :----------------------: | :----------------------: |
| $\to$ a         | b                        | b                        |
| b               | c                        | c                        |
| c               | d                        | d                        |
| d               | d                        | d                        |

Table: Transition Table

- Construct a DFA that accepts set of all strings over $\sum = {a,b} \ \mid \ w \ \geq 2$

![](gv/dfa2.png){width=50%}

- Construct a DFA that accepts set of all strings over $\sum = {a,b} \ \mid \ w \ \leq 2$

![](gv/dfa3.png){width=50%}

- Construct a DFA that accepts set of all strings over $\sum = {a,b} \ \mid \abs{w} \mod 2 = 0$

![](gv/dfa4.png){width=50%}

- Construct a DFA that accepts set of all strings over $\sum = {a,b} \ \mid \ a(w) = 2$

![](gv/dfa5.png){width=50%}

- Construct a DFA that accepts set of all strings over $\sum = {a,b} \ \mid \ a(w) \mod 2 = 0$

![](gv/dfa6.png){width=50%}

- Construct a DFA that accepts set of all strings over $\sum = {a,b} \ \mid \ a(w) \mod 2 = 0 \text{ and } b(w) \mod 2 = 0$

![](gv/dfa7.png){width=50%}

- Construct a DFA that accepts set of all strings over $\sum = {a,b} \ \mid \abs{w} \mod 3 = 0$

![](gv/dfa8.png){width=50%}

- Construct a DFA that accepts set of all strings over $\sum = {a,b} \ \mid \abs{w} \mod 3 = 1$

![](gv/dfa9.png){width=50%}

- Construct a DFA that accepts set strings starting with `a`

![](gv/dfa10.png){width=50%}

- Construct a DFA that accepts set of all strings containing `a`

![](gv/dfa11.png){width=50%}

- Construct a DFA that starts with `a` and ends with `b`

![](gv/dfa12.png){width=50%}

- Construct a DFA that accepts all strings starting and ending with different symbols

![](gv/dfa13.png){width=50%}

- Construct a DFA that accepts all strings starting and ending with same symbols

![](gv/dfa14.png){width=50%}

- Construct a DFA that accepts all strings with $a^n b^m \mid n,m \geq 1$

![](gv/dfa15.png){width=50%}

- Construct a DFA that accepts all strings with $a^n b^m \mid n,m \geq 0$

![](gv/dfa16.png){width=50%}
