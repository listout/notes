# Different Types of Grammar

![Typef of Grammar](cs/types_of_grammar.png){width=50%}

## Examples

- Regular Language

$$a^n \mid n \geq 1$$

- Context Free Language

$$a^n b^n \mid n \geq 1$$

- Context Sensitive Language

$$a^n b^n c^n \mid n \geq 1$$

- Recursively Enumerable Set

$$a^n \mid p \ \text{is prime}$$
