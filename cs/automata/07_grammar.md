# Grammar

A phrase structure grammar (or simply just a grammar) is $V_n$, $\sum$, $P$ and $S$; where

- $V_n$ is a finite non-empty set whose elements are called variables.
- $\sum$ is as finite non-empty set whose elements are called terminals.
- $V_n \cap \sum$ = $\phi$
- $S$ is a special variable that is an element of the $V_n$ called the start symbol.
- $P$ is finite set of whose elements are $a \to b$, a and b are strings on $V_n \cap \sum$.`a` has at least on symbol from $V_n$. The elements of $P$ are called production rules.

## Example

$$G(V_n, \sum, P, S)$$

$$S \to aSB \text{Intermidiate} \to \text{working}$$

$$S \to aB \text{final} \to \text{sentese}$$

$$B \to b$$

$$V_n = {S,B}, \sum = {a,b}$$

![In tree form](cs/regex_eg1.png){width=50%}
