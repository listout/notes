# Automata

## What is automata

An automata is defined as a system where energy, material and information are transformed, transmitted and used for performing some function without direct participation of man.

![Automaton](gv/automaton.png){width=50%}

The characteristics of automaton are:

- *Input*: At each of the discrete instants of the time $t_1, t_2, \dots , t_m$ the input values $i_1, i_2, \dots , i_p$ each of which can take a finite number of fixed values from the input alphabet $\sum$, are applied to the input side of the model
-  *Output*: $o_1, o_2, \dots, o_n$ are the outputs of the model, each of which can take a finite number of fixed values from the output O
-  *States*: At any instant of the time the automaton can be in one of the states $q_1, q_2, \dots , q_n$
-  *State Relation*: The next state of an automaton at any instant of the time is determined by the present state and the present input.
-  *Output Relation*: The output is related to either state only or to both th input and the state. It should be noted that at any instant of the time the automaton is in some state. On 'reading' an input symbol, the automaton moves to the next state which is given by the state relation.

Examples include automatic machine tools, automatic packaging machines and automatic photocopying machines.
