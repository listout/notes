# Regular Expression Representation

![Types of Regualar Language](cs/regex_family.png){width=50%}

## Examples

Find out the RegEx for $\sum$ = {a, b} of length exactly 2

L = {aa, ab, bb, ba}

RegEx: $(a+b)$

RegEx for $\sum$ of length $\geq$ 2

L = {aa, ab, bb, aaa, $\ldots$}

RegEx: $(a+b)(a+b)(a+b)^\star$

## Rules for RegEx

- $\phi$ + R = R + $\phi$ = R
- $\phi$ . R = R .$\phi$ = R
- $\varepsilon$ . R = R . $\varepsilon$ = R
- $\varepsilon^\star$ = $\varepsilon$
- $\varepsilon$ + R$R^\star$ = R$R^\star$ + $\varepsilon$ = $R^\star$
