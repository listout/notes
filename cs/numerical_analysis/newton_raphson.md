# Newton Raphson

## Formula

Supposing we want to find root of of a continuous, differentiable function
$f(x)$, and we know the root is near the point $x = x_0$. Then newtons method
tells us that a better approximation of the root would be

$$
x_1 = x_0 - \frac{f(x_0)}{f^\prime(x_0)}
$$

The process may be repeated as many times as necessary.

$$
x_{n+1} = x_n - \frac{f(x_n)}{f^\prime(x_n)}
$$

## Code

```{.c}
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// the function and it's derivative, should be changed as necessary
#define f(x) (3 * x - cos(x) - 1)
#define df(x) (3 + sin(x))

int
main(void)
{
	double ini_guess, tol_level; // initial guess and tolerance level
	int iter, x_num = 1;         // number of iterations

	scanf("%lf %lf", &ini_guess, &tol_level);
	scanf("%d", &iter);

	printf("Values per iteration\n");
	while(iter--) {
		// calculating values
		double temp = ini_guess - (f(ini_guess) / df(ini_guess));

		if(fabs(f(ini_guess) / df(ini_guess)) < tol_level) {
			// break if exceeds tolerance level
			break;
		}
		printf("At x_%d %lf\n", x_num++, temp);

		// substitution values for next iteration
		ini_guess = temp;
	}

	printf("Final answer: x_%d %lf", --x_num, ini_guess);

	return 0;
}
```

## Output

```
$ ./bin/newtonRapson < input03
Values per iteration
At x_1 0.620016
At x_2 0.607121
At x_3 0.607102
Final answer: x_3 0.607102
```

```
Data:
1 0.00001
10
```
