---
title: Numberical Analysis
documentclass: report
toc: true
---

# Newton Interpolation

## Formula

**Forward**

$$
y(x) = y_0 + p \Delta y_0 + \frac{p(p-1)}{2!}  \Delta^2 y_0 + \frac{p(p+1)(p+2)}{3!} \Delta^3 y_0 + \ldots
\ \text{where} \ p = \frac{x - x_0}{h} \ \text{and} \ h = x_1 - x_0
$$

**Backward**

$$
y(x) = y_n + p \nabla y_n + \frac{p(p-1)}{2!}  \nabla^2 y_n + \frac{p(p+1)(p+2)}{3!} \nabla^3 y_n + \ldots
\ \text{where} \ p = \frac{x - x_n}{h} \ \text{and} \ h = x_1 - x_0
$$

## Code

```{.c}
#include <stdbool.h>
#include <stdio.h>

int
factorial(int number)
{
	int temp = 1;
	for(int i = 1; i <= number; ++i)
		temp = temp * i;
	return temp;
}

// calculation of `u`, sometimes called `p`
double
cal_u(int i, double u, bool flag)
{
	double temp = 1;
	if(flag) {
		for(int j = 1; j <= i; ++j)
			temp = temp * (u - j);
	} else {
		for(int j = 1; j <= i; ++j)
			temp = temp * (u + j);
	}
	return temp;
}

int
main(void)
{
	int data_n;
	scanf("%d", &data_n); // number of data
	int data_x[data_n];
	double data_fx[data_n][data_n];

	for(int i = 0; i < data_n; ++i) { // X[i] elements
		scanf("%d %lf", &data_x[i], &data_fx[i][0]);
	}
	int diff = data_x[1] - data_x[0]; // known as the `h`

	double interpolate_value;
	scanf("%lf", &interpolate_value); // interpolation value

	// filling the difference table
	for(int i = 1; i < data_n; ++i) {
		for(int j = 0; j < data_n - i; ++j)
			data_fx[j][i] = data_fx[j + 1][i - 1] - data_fx[j][i - 1];
	}

	// performing interpolation based on interpolate_value
	double sum = 0;
	if(interpolate_value >= data_x[(int)((data_n - 1) / 2)]) {
		printf("Backward Interpolation \n");
		double u = (interpolate_value - data_x[data_n - 1]) / diff;
		sum      = data_fx[data_n - 1][0] + (u * data_fx[data_n - 2][1]);
		for(int i = 2, j = data_n - 3; i < data_n; ++i, j--)
			sum += (u * cal_u(i - 1, u, false) * data_fx[j][i]) / factorial(i);
	} else {
		printf("Forward Interpolation\n");
		double u = (interpolate_value - data_x[0]) / diff;
		sum      = data_fx[0][0] + (u * data_fx[0][1]);
		for(int i = 2; i < data_n; ++i)
			sum += (u * cal_u(i - 1, u, true) * data_fx[0][i]) / factorial(i);
	}

	printf("x and f(x)\n");
	for(int i = 0; i < data_n; ++i) {
		printf("%3d %3.0lf", data_x[i], data_fx[i][0]);
		printf("\n");
	}

	printf("\nDifference Table\n");
	for(int i = 0; i < data_n; ++i) {
		for(int j = 0; j < data_n - i; ++j)
			printf("%3.0lf", data_fx[i][j]);
		printf("\n");
	}

	printf("\nf(%lf) is : %lf\n", interpolate_value, sum);
	return 0;
}
```

## Output

```
$ ./bin/newtonInterpolation < input01
Forward Interpolation
x and f(x)
1891  46
1901  66
1911  81
1921  93
1931 101

Difference Table
 46 20 -5  2 -3
 66 15 -3 -1
 81 12 -4
 93  8
101

f(1895.000000) is : 54.852800



$ ./bin/newtonInterpolation < input02
Backward Interpolation
x and f(x)
  0   1
  1   0
  2   1
  3  10

Difference Table
  1 -1  2  6
  0  1  8
  1  9
 10

f(4.000000) is : 33.000000
```

```
Data:
Forward:
5
1891 46
1901 66
1911 81
1921 93
1931 101
1895

Backward:
4
0 1
1 0
2 1
3 10
4
```
