# Bisection Method

## Formula

The input for the method is a continuous function $f$, an interval $[a, b]$ and
the function $f(a)$ and $f(b)$. The function values are of opposite sign. Each
iteration performs these steps:

- Calculate `c`, the midpoint of the interval, $c = \frac{a+b}{2}$.
- Calculate the function value at midpoint, $f(c)$.
- If convergence is satisfactory ($c - a$ is sufficiently small, or $\mid f(c)
\mid$ is sufficiently small), return c and stop iterating.
- Examine the sing of $f(c)$ and replace either $(a, f(a))$ or $(b, f(b))$ with
$(c, f(c))$ so that there are zero crossing withing the new interval.

## Code

```{.c}
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define f(x) pow(x, 3) - x - 2 // the function, change as required

int
main(void)
{
	double a, b; // endpoint values
	scanf("%lf %lf", &a, &b);

	// conditions
	if((f(a) < 0 && f(b) > 0) || (f(a) > 0 && f(b) < 0)) {
		printf("Condition satisfied\n");
	} else {
		printf("Condition not satisfied, exiting code\n");
		exit(1);
	}

	// tolerance level
	double tol;
	scanf("%lf", &tol);

	// maximum number of iterations
	int maxIter, counter = 1;
	scanf("%d", &maxIter);

	while(counter <= maxIter) {
		double c = (a + b) / 2;
		printf("Iteration: %2d a_n: %1.6lf b_n: %1.6lf c_n: %1.6lf f(c_n): %1.6lf\n",
		       counter++,
		       a,
		       b,
		       c,
		       f(c));

		if(f(c) == 0 || (b - a) / 2 < tol) {
			printf("%lf\n", c);
			return 0;
		}

		if(f(c) < 0 && f(a) < 0)
			a = c;
		else
			b = c;
	}

	// if exists loop, then condition failed
	printf("Method failed\n");
	return 0;
}
```

## Output

```
$ ./bin/bisection < input04
Condition satisfied
Iteration:  1 a_n: 1.000000 b_n: 2.000000 c_n: 1.500000 f(c_n): -0.125000
Iteration:  2 a_n: 1.500000 b_n: 2.000000 c_n: 1.750000 f(c_n): 1.609375
Iteration:  3 a_n: 1.500000 b_n: 1.750000 c_n: 1.625000 f(c_n): 0.666016
Iteration:  4 a_n: 1.500000 b_n: 1.625000 c_n: 1.562500 f(c_n): 0.252197
Iteration:  5 a_n: 1.500000 b_n: 1.562500 c_n: 1.531250 f(c_n): 0.059113
Iteration:  6 a_n: 1.500000 b_n: 1.531250 c_n: 1.515625 f(c_n): -0.034054
Iteration:  7 a_n: 1.515625 b_n: 1.531250 c_n: 1.523438 f(c_n): 0.012250
Iteration:  8 a_n: 1.515625 b_n: 1.523438 c_n: 1.519531 f(c_n): -0.010971
Iteration:  9 a_n: 1.519531 b_n: 1.523438 c_n: 1.521484 f(c_n): 0.000622
Iteration: 10 a_n: 1.519531 b_n: 1.521484 c_n: 1.520508 f(c_n): -0.005179
Iteration: 11 a_n: 1.520508 b_n: 1.521484 c_n: 1.520996 f(c_n): -0.002279
Iteration: 12 a_n: 1.520996 b_n: 1.521484 c_n: 1.521240 f(c_n): -0.000829
Iteration: 13 a_n: 1.521240 b_n: 1.521484 c_n: 1.521362 f(c_n): -0.000103
Iteration: 14 a_n: 1.521362 b_n: 1.521484 c_n: 1.521423 f(c_n): 0.000259
1.521423

Data:
1 2
0.0001
15
```
